/** @format */

import React from "react";
import Upload from "../assets/icon/fi_upload.svg";

function UploadImgae({ ...rest }) {
  return (
    <>
      <div class='row'>
        <div class='col-lg-3'>
          <label for='inputFoto6' class='col-form-label'>
            Foto
          </label>
          <label
            for='wajib'
            // style='color: red'
          >
            *
          </label>
        </div>
        <div class='col-lg-5'>
          <div class='input-group'>
            <input
              class='form-control'
              type='file'
              id='formFile'
              // style='display: none'
              {...rest}
            />
            <span class='input-group-text'>
              <img src={Upload} />
            </span>
          </div>
          <small
            id='fileHelp'
            class='form-text text-muted'
            // style='margin-left: 1px'
          >
            File size max. 2 MB
          </small>
        </div>
      </div>
    </>
  );
}

export default UploadImgae;
