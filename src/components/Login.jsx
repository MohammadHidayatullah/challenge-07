/** @format */

import React from "react";
import { GoogleLogin } from "react-google-login";
import { useNavigate } from "react-router";
function Login() {
  const clientId =
    "1092107112629-at7v71f30auqc6onspvj2ia11r5icj9c.apps.googleusercontent.com";

  const navigate = useNavigate();

  const onSuccess = (res) => {
    localStorage.setItem("access_token", res.tokenObj.id_token);
    navigate("/home", { replace: true });
    // console.log("LOGIN SUCCES! current user: ", res.profileObj);
  };

  const onFailure = (res) => {
    console.log("LOGIN FAILED! res: ", res);
  };

  return (
    <div id='signInButton'>
      <GoogleLogin
        clientId={clientId}
        buttonText='Login'
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        // isSignedIn={true}
      />
    </div>
  );
}

export default Login;
