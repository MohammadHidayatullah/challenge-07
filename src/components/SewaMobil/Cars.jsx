/** @format */

import React, { useEffect, useState } from "react";
import Fikey from "../../assets/icon/fi_key.svg";
import Ficlock from "../../assets/icon/fi_clock.svg";
import NumberFormat from "react-number-format";
import "../../assets/css/Cars.css";
import { useNavigate } from "react-router";
import axios from "axios";

const baseURL = "https://rent-car-appx.herokuapp.com/admin/car";
function Cars() {
  const [cars, setCars] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    const getCars = async () => {
      const { data: res } = await axios.get(baseURL);
      setCars(res);
    };
    getCars();
  }, []);
  const handleGoToDetail = (id) => {
    navigate(`/sewa/${id}`);
  };
  return (
    <>
      <section className='content-section'>
        <div className='row'>
          <div className='col-lg-12' id='show-col-lg-12'>
            <div className='container-fluid'>
              <div className='row g-0'>
                {cars.map((cars) => {
                  {
                    /* {cars
                  .filter((car) => car.status === true)
                  .map((car) => { */
                  }
                  return (
                    <div className='col'>
                      <div
                        className='card'
                        onClick={() => handleGoToDetail(cars.id)}>
                        <div className='card-body'>
                          <h5 className='card-title'>
                            <img
                              src={cars.image}
                              className='img-car'
                              alt='img-car'
                            />
                          </h5>
                          <p>
                            {cars.name}/{cars.category}
                          </p>
                          <h6>
                            <NumberFormat
                              value={cars.price}
                              displayType={"text"}
                              thousandSeparator={true}
                              prefix={"Rp "}
                            />{" "}
                            / hari
                          </h6>
                          <p className='card-text'>
                            <img src={Fikey} alt='icon-key' />
                            {cars.start_rent_at} - {cars.finish_rent_at}
                          </p>
                          <p className='card-text'>
                            <img src={Ficlock} alt='icon-clock' />
                            {cars.updatedAt}
                          </p>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
      ;
    </>
  );
}

export default Cars;
