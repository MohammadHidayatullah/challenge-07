/** @format */

import React from "react";
import "../assets/css/NavbarDashboard.css";
import Rectangle6 from "../assets/icon/Rectangle 6.svg";
import Rectangle63 from "../assets/icon/Rectangle 63.svg";
import HomeIcon from "../assets/icon/Home_Icon.svg";
import TruckIcon from "../assets/icon/Truck_Icon.svg";
import Group15 from "../assets/icon/Group 15.svg";
import FiMenu from "../assets/icon/fi_menu.svg";
import { Dropdown } from "react-bootstrap";

function NavbarDashboard() {
  const handleLogout = async () => {
    localStorage.clear();
    window.location.href = "/";
  };
  return (
    <>
      <section className='navbar-section'>
        <nav className='navbar navbar-expand-lg border-bottom ms-5'>
          <div className='container-fluid'>
            <div className='sidebar-toggler ps-5'>
              <a href='#'>
                <img className='ms-5 me-5 collapseSidebar' src={Rectangle6} />
              </a>
              <button className='btn ms-5' id='toggleSidebar'>
                <img src={FiMenu} />
              </button>
            </div>
            <button
              className='navbar-toggler'
              type='button'
              data-bs-toggle='collapse'
              data-bs-target='#navbarSupportedContent'
              aria-controls='navbarSupportedContent'
              aria-expanded='false'
              aria-label='Toggle navigation'>
              <img src={FiMenu} alt='' />
            </button>
            <div className='collapse navbar-collapse' id='navbarSupportedContent'>
              <ul className='navbar-nav ms-auto mt-2 mt-lg-3'>
                <li className='nav-item'>
                  <form className='d-flex ms-5'>
                    <input
                      className='form-control'
                      type='search'
                      placeholder='Search'
                      aria-label='Search'
                    />
                    <button
                      className='btn btn-outline-primary border-3 me-3 fw-bold'
                      type='submit'>
                      Search
                    </button>
                  </form>
                </li>
                <li className='nav-item'>
                  <div className='dropdown'>
                      {/* <img
                        src='images/icon/Group 15.svg'
                        width='32'
                        height='32'
                        class='rounded-circle me-2'
                        style='object-fit:cover;'
                      /> */}

                      <Dropdown >
                        <Dropdown.Toggle className="d-flex flex-row justify-content-center align-items-center link-dark text-decoration-none  me-2" variant='custom' id='dropdown-basic' >
                        <p className='my-2 me-2'>Unis Badri</p>
                        <img
                        src={Group15}
                        width='32'
                        height='32'
                        className='rounded-circle me-2'
                      />
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                          <Dropdown.Item onClick={handleLogout}>
                            Logout
                          </Dropdown.Item>
                          </Dropdown.Menu>
                      </Dropdown>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </section>
      <section className='sidebar-section'>
        {/* Sidebar - Main Menu */}
        <div className='main-menu'>
          <a href='/dashboard'>
            <div className='box text-center d-flex justify-content-center align-items-center'>
              <img className='side-icon' src={Rectangle63} />
            </div>
          </a>
          <a href='/dashboard'>
            <div className='box text-center py-2'>
              <img className='side-icon' src={HomeIcon} />
              <div>Dashboard</div>
            </div>
          </a>
          <a href='/cars'>
            <div className='box text-center py-2'>
              <img className='side-icon' src={TruckIcon} />
              <div>Cars</div>
            </div>
          </a>
        </div>
        <div className='collapseSidebar'>
          {/* Sidebar - Menu List */}
          <div className='menu-list pt-3'>
            <h4 className='text-secondary px-3 py-2'>CARS</h4>
            <a href='/dashboard'>
              <div className='list-menu'>
                <p className='m-0'>Dashboard</p>
              </div>
            </a>
          </div>
        </div>
      </section>
    </>
  );
}

export default NavbarDashboard;
