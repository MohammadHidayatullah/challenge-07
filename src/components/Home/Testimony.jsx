/** @format */

import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);
export const data = {
  labels: ["Innova", "yaris", "Avanza", "L300"],
  datasets: [
    {
      label: "# of Votes",
      data: [124, 400, 50, 20],
      backgroundColor: [
        "yellow",
        "blue",
        "green",
        "violet",
      ],
      borderColor: [
        "black",
      ],
      borderWidth: 2,
    },
  ],
};

function Testimony() {
  return (
    <>
      <section id='testimony'>
        <div class='text-center'>
          <h2>Testimonial</h2>
          <p class='info'>Berbagai review positif dari para pelanggan kami</p>
        </div>
        <div className='chart'>
          <Pie data={data} />;
        </div>
      </section>
    </>
  );
}

export default Testimony;
