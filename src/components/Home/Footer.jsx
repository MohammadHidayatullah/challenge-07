/** @format */

import React from "react";
import Facebook from '../../assets/icon/home/sosmed/icon_facebook.svg'
import Instagram from '../../assets/icon/home/sosmed/icon_instagram.svg'
import Twitter from '../../assets/icon/home/sosmed/icon_twitter.svg'
import Mail from '../../assets/icon/home/sosmed/icon_mail.svg'
import Twitch from '../../assets/icon/home/sosmed/icon_twitch.svg'
import Logo from '../../assets/icon/home/img/logo.png'

function Footer() {
  return (
    <>
      <section id='footer'>
        <div class='container'>
          <div class='row'>
            <div class='address col-lg-3 col-md-6 col-12 mb-3'>
              <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
              <p>binarcarrental@gmail.com</p>
              <p>081-233-334-808</p>
            </div>
            <div class='nav-bottom col-lg-2 col-md-6 col-12 mb-3'>
              <a class='nav-link-bottom' href='#service'>
                Our Services
              </a>
              <a class='nav-link-bottom' href='#whyus'>
                Why Us
              </a>
              <a class='nav-link-bottom' href='#testimony'>
                Testimonial
              </a>
              <a class='nav-link-bottom' href='#faq'>
                FAQ
              </a>
            </div>
            <div class='sosmed col-lg-4 col-md-6 col-12 mb-3'>
              <p>Connect with us</p>

              <a class='sos-link' href='#'>
                <img src={Facebook} alt='' />
              </a>

              <a class='sos-link' href='#'>
                <img src={Instagram} alt='' />
              </a>

              <a class='sos-link' href='#'>
                <img src={Twitter} alt='' />
              </a>

              <a class='sos-link' href='#'>
                <img src={Mail} alt='' />
              </a>

              <a class='sos-link' href='#'>
                <img src={Twitch} alt='' />
              </a>
            </div>
            <div class='copyright col-lg-3 col-md-6 col-12 mb-3'>
              <p>Copyright Binar 2022</p>
              <a class='navbar-brand' href='#'>
                <img src={Logo} alt='' />
              </a>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Footer;
