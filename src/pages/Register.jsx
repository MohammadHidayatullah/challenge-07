/** @format */

import "../assets/css/Register.css";
import Image2 from "../assets/img/image 2.svg";
import Rectangle6 from "../assets/icon/Rectangle 62.svg";
import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import axios from "axios";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import LoginButton from "../components/Login";
import { gapi } from "gapi-script";

const clientId =
  "1092107112629-at7v71f30auqc6onspvj2ia11r5icj9c.apps.googleusercontent.com";

function Register() {
  const [registerData, setRegisterData] = React.useState({
    email: "",
    password: "",
    role: "Customer",
    // email: "user@mail.com",
    // password: "123456",
  });

  const navigate = useNavigate();

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }
    gapi.load("client:auth2", start);
  });

  // var access_token = gapi.auth.getToken().access_token;

  useEffect(() => {
    if (localStorage.getItem("access_token")) {
      navigate("/");
    }
  }, []);

  const handleRegister = async () => {
    try {
      const res = await axios({
        method: "POST",
        url: "https://rent-car-appx.herokuapp.com/admin/auth/register",
        data: registerData,
      });
      // console.log(res);
      console.log(res.data);

      navigate(
        "/",
        alert(
          "anda berhasil mendaftarkan akun baru, silahkan login dengan akun yang telah terdaftar"
        )
      );
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <section class='login vh-100'>
        <div class='container-fluid'>
          <div class='row login'>
            <div class='col-7 left-side'>
              <img src={Image2} class='banner-login w-100 vh-100' alt='' 
                style={{ objectFit: "cover" }}
              />
            </div>
            <div class='content col-3'>
              <div class='form-head'>
                <img src={Rectangle6} alt='' />
                <h1>Sign Up</h1>
              </div>

              <div class='form-content'>
                <Form.Group action=''>
                  <div class='mb-3'>
                    <Form.Label for='inputEmail' class='form-label'>
                      Email
                    </Form.Label>
                    <Form.Control
                      type='email'
                      class='form-control'
                      id='inputEmail'
                      placeholder='Contoh: johndee@gmail.com'
                      value={registerData.email}
                      onChange={(e) =>
                        setRegisterData({
                          ...registerData,
                          email: e.target.value,
                        })
                      }
                    />
                  </div>
                  <div class='mb-3'>
                    <Form.Label for='inputPassword' class='form-label'>
                      Password
                    </Form.Label>
                    <Form.Control
                      type='password'
                      class='form-control'
                      id='inputPassword'
                      placeholder='6+ karakter'
                      value={registerData.password}
                      onChange={(e) =>
                        setRegisterData({
                          ...registerData,
                          password: e.target.value,
                        })
                      }
                    />
                  </div>

                  <a
                    class='btn btn-primary'
                    role='button'
                    onClick={handleRegister}>
                    Sign Up
                  </a>
                  <p>
                    Already have an account? <Link to={"/"}>Login</Link>
                  </p>
                  <LoginButton />
                </Form.Group>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Register;
