/** @format */
import React, { useEffect, useState } from "react";
import Modal from "../components/Modal";
import NavbarDashboard from "../components/NavbarDashboard";
import Fikey from "../assets/icon/fi_key.svg";
import Ficlock from "../assets/icon/fi_clock.svg";
import Fitrash from "../assets/icon/fi_trash-2.svg";
import Fiedit from "../assets/icon/fi_edit.svg";
import Fiplus from "../assets/icon/fi_plus.svg";
import NumberFormat from "react-number-format";
import "../assets/css/Cars.css";
import { useNavigate } from "react-router";
import axios from "axios";

const baseURL = "https://rent-car-appx.herokuapp.com/admin/car";
function Cars() {
  const [cars, setCars] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    const getCars = async () => {
      const { data: res } = await axios.get(baseURL);
      setCars(res);
    };
    getCars();
  }, []);
  const handleGoToDetail = (id) => {
    navigate(`/cars/${id}`);
  };

  const handleDelete = async (e) => {
    try {
      await axios.delete(`${baseURL}/${e}`);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <NavbarDashboard />
      <section className='content-section ps-5 pe-4'>
        <div className='row'>
          <div className='col-lg-0' id='show-col-lg-0'></div>
          <div className='col-lg-12' id='show-col-lg-12'>
            <div className='container-fluid'>
              <div className='list-button'>
                <span className='list-car'>List Car</span>
                <div className='button-right'>
                  <a href='/addCar' className='btn btn-primary'>
                    <img src={Fiplus} alt='icon-plus' /> Add New Car
                  </a>
                </div>
              </div>
              <div className='btn-group' aria-label='Basic example'>
                <button type='button' className='btn btn-outline-primary'>
                  All
                </button>
                <button type='button' className='btn btn-outline-primary'>
                  Small
                </button>
                <button type='button' className='btn btn-outline-primary'>
                  Medium
                </button>
                <button type='button' className='btn btn-outline-primary'>
                  Large
                </button>
              </div>
              <div className='row g-0'>
                {cars.map((cars) => {
                  {
                    /* {cars
                  .filter((car) => car.status === true)
                  .map((car) => { */
                  }
                  return (
                    <div className='col'>
                      <div className='card'>
                        <div className='card-body'>
                          <h5 className='card-title'>
                            <img
                              src={cars.image}
                              className='img-car'
                              alt='img-car'
                            />
                          </h5>
                          <p onClick={() => handleGoToDetail(cars.id)}>
                            {cars.name}/{cars.category}
                          </p>
                          <h6>
                            <NumberFormat
                              value={cars.price}
                              displayType={"text"}
                              thousandSeparator={true}
                              prefix={"Rp "}
                            />{" "}
                            / hari
                          </h6>
                          <p className='card-text'>
                            <img src={Fikey} alt='icon-key' />
                            {cars.start_rent_at} - {cars.finish_rent_at}
                          </p>
                          <p className='card-text'>
                            <img src={Ficlock} alt='icon-clock' />
                            {cars.updatedAt}
                          </p>
                          <div className='btn-group' aria-label='Basic example'>
                            {/* Button trigger modal */}
                            <button
                              onClick={() => handleDelete(cars.id)}
                              type='button'
                              className='btn btn-danger'
                              data-toggle='modal'
                              data-target='#exampleModal'>
                              <img src={Fitrash} alt='icon-plus' />
                              Delete
                            </button>
                            <a href='/editCar' className='btn btn-success'>
                              <img src={Fiedit} alt='icon-edit' />
                              Edit
                            </a>
                            <Modal />
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
      ;
    </>
  );
}

export default Cars;
