/** @format */

import React, { useState } from "react";
import Navbar from "../components/Navbar";
import FiArrowLeft from "../assets/icon/fi_arrow-left.svg";
import FiCheck from "../assets/icon/fi_check.svg";
import Rectangle36 from "../assets/icon/Rectangle 36.svg";
import Success from "../assets/icon/success.svg";
import Download from "../assets/icon/fi_download.svg";
import "../assets/css/Tiket.css";
import { Viewer, Worker } from "@react-pdf-viewer/core";
import file from "../assets/pdf/document.pdf";
import { getFilePlugin } from "@react-pdf-viewer/get-file";

function Tiket() {
  const [PdfFile] = useState(file);
  const getFilePluginInstance = getFilePlugin();
  const { DownloadButton } = getFilePluginInstance;
  return (
    <>
      <Navbar />
      <section className='tiket-section'>
        <div className='container'>
          <div className='row header'>
            <div className='col-auto'>
              <a href='#'>
                <img src={FiArrowLeft} alt='' /> Tiket{" "}
              </a>
              <p>Order : 011235</p>
            </div>
            <div className='col-auto'>
              <ul className='list-menu d-flex flex-row'>
                <li>
                  <img className='fi-check' src={FiCheck} alt='' />
                  Pilih Metode
                </li>
                <li>
                  <img src={Rectangle36} alt='' />
                </li>
                <li>
                  <img className='fi-check' src={FiCheck} alt='' />
                  Bayar
                </li>
                <li>
                  <img src={Rectangle36} alt='' />
                </li>
                <li>
                  <img className='fi-check' src={FiCheck} alt='' />
                  Tiket
                </li>
              </ul>
            </div>
          </div>
          <div className='row content'>
            <div className='col-md-7'>
              <div className='success text-center'>
                <img src={Success} alt='' />
                <p className='content-title mt-2'>Pembayaran Berhasil</p>
                <p className='content-description'>
                  Tunjukan invoice ini kepada petugas BCR di titik temu
                </p>
              </div>
              <div className='box'>
                <div className='row'>
                  <div className='col-auto'>
                    <p className='content-title'>Invoice</p>
                    <p className='content-description2'>*no invoice</p>
                  </div>
                  <div className='col-auto unduh'>
                    <button className='btn btn-download'>
                      <DownloadButton
                        style={{ backgroundImage: `url(${Download})` }}
                        className='pdf-download'>
                        <img
                          src={process.env.PUBLIC_URL + 'assets/icon/fi_download.svg'}
                          className='btButton'
                          alt='Download'
                        />
                      </DownloadButton>
                    </button>
                  </div>
                  <div className='box-viewer text-center'>
                    <Worker workerUrl='https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js'>
                      <Viewer
                        fileUrl={PdfFile}
                        plugins={[getFilePluginInstance]}
                      />
                    </Worker>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Tiket;
